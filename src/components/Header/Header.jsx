import React, { useRef } from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import CodeTwoToneIcon from '@material-ui/icons/CodeTwoTone';
import { darkGrey } from '../../assets/colors.scss';
import RightSidebar from '../RightSidebar/RightSidebar';
import AnimatedNumber from '../AnimatedNumber/AnimatedNumber';
import PropTypes from 'prop-types';

const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1
  },
  cursor: {
    cursor: 'pointer'
  },
  headerContainer: {
    padding: '25px 25px 10px 25px',
    position: 'fixed',
    margin: '0% auto' /* Will not center vertically and won't work in IE6/7. */,
    left: 0,
    right: 0,
    top: 0,
    zIndex: 9,
    backgroundColor: '#FFF',
    boxShadow: '0 1px 6px 0 rgba(32,33,36,0.28)'
  }
}));

function Header(props) {
  const childRef = useRef();
  const classes = useStyles();

  return (
    <Grid className={classes.headerContainer}>
      <div className={classes.root}>
        <Grid container spacing={3}>
          <Grid item xs={3}>
            <CodeTwoToneIcon
              fontSize='large'
              htmlColor={darkGrey}
              onClick={() => childRef.current.toggleDrawer('right', true)}
              className={classes.cursor}
            />
            <RightSidebar ref={childRef} history={props.history} />
          </Grid>
          <Grid item xs={9}>
            <AnimatedNumber type="simple-text" text={'Vivek Rajoriya'} />
          </Grid>
        </Grid>
      </div>
    </Grid>
  );
}

export default Header;

Header.propTypes = {
  history: PropTypes.object.isRequired
};
