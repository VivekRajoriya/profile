import React from 'react';
import './ConnectWithMe.scss';
import PropTypes from 'prop-types';
import { Box, Button } from '@material-ui/core';

export default function ConnectWithMe(props) {
  return (
    <Box
      className={`${props.title === 'Ask for apk' ? '' : 'connect-with-me'}`}>
      {props.title === 'Ask for apk' ? (
        <Button
          variant='outlined'
          style={{ margin: 8, marginLeft: 0 }}
          onClick={() =>
            window.open(
              'mailto:vivekrajoriya106@gmail.com?subject=Regarding:Request for tiffin box APK&body=Hey vivek, can you please share tiffin box apk for checkin at my end. Thanks',
              '_self'
            )
          }>
          {props.title}
        </Button>
      ) : (
        <Button
          variant='outlined'
          color='primary'
          onClick={() =>
            window.open(
              'mailto:vivekrajoriya106@gmail.com?subject=Regarding:&body=Hey vivek',
              '_self'
            )
          }>
          {props.title}
        </Button>
      )}
    </Box>
  );
}

ConnectWithMe.propTypes = {
  title: PropTypes.string.isRequired
};
