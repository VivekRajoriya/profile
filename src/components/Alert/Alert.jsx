import React from 'react';
import { useSnackbar } from 'notistack';
import PropTypes from 'prop-types';

export default function ShowAlert(props) {
  const { enqueueSnackbar } = useSnackbar();

  const handleClickVariant = (msg, variant) => {
    // variant could be success, error, warning, info, or default
    return enqueueSnackbar(msg, { variant });
  };

  return (
    <>
      {props.type === 'success'
        ? handleClickVariant(props.msg, props.type)
        : ''}
    </>
  );
}

ShowAlert.propTypes = {
  type: PropTypes.string.isRequired,
  msg: PropTypes.string.isRequired
};
