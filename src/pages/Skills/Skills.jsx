import React from 'react';
import { Container, Grid, Avatar } from '@material-ui/core';
import HelloWorld from '../../assets/images/hello-world.png';
import './Skills.scss';
import SimpleFadeCard from './Cards/Cards';
import CustomButton from '../../components/CustomButton/CustomButton';

class Skills extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      skills: [
        {
          item: ['Javascript', 'C', 'C++', 'Java'],
          fieldEnable: false,
          name: 'Programming Language'
        },
        {
          item: [
            'Google Map',
            'Google Tag Manager',
            'firebase (Push notification and otp verification)',
            'Virtual scroll',
            'Infinite scroll',
            'Redux',
            'JWT',
            'NgRx',
            'Rxjs',
            'material ui',
            'angular material',
            'react-bootstrap',
            'reactstrap',
            'others...'
          ],
          fieldEnable: false,
          name: 'Libraries'
        },
        {
          item: ['Reactjs', 'Angular', 'Ionic', 'Express'],
          fieldEnable: false,
          name: 'Frameworks'
        },
        { item: ['MongoDB'], fieldEnable: false, name: 'Database' },
        { item: ['Git'], fieldEnable: false, name: 'VersionControl' },
        {
          item: ['Trello', 'Jira', 'Bitbucket', 'Gitlab'],
          fieldEnable: false,
          name: 'Project Management Tool'
        },
        {
          item: ['Visual studio code', 'Atom', 'sublime', 'notepad++'],
          fieldEnable: false,
          name: 'CodeEditor'
        }
      ]
    };
  }

  render() {
    return (
      <Container className='skills-container'>
        <Grid container justify='center' alignItems='center'>
          <Avatar alt='skills-img' src={HelloWorld} className='bigAvatar' />
        </Grid>
        <SimpleFadeCard skillSet={this.state.skills} />
        <CustomButton btnName='Work Experience' type='secondary' />
      </Container>
    );
  }
}

export default Skills;
