/* eslint-disable react/no-array-index-key */
import React from 'react';
import './WorkExp.scss';
import { Container, Box, Typography, Grid, Avatar } from '@material-ui/core';
import developer from '../../assets/images/developer.jpg';
import CustomButton from '../../components/CustomButton/CustomButton';
import '../../assets/colors.scss';
import ListData from './List/List';
import { projectConstant } from '../../utils/projectConstant';

class WorkExp extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      workExp: projectConstant.workExp
    };
  }

  render() {
    return (
      <Container className='WorkExp-container'>
        <Grid container alignItems='flex-start' key='#work-exp'>
          <Avatar alt='skills-img' src={developer} className='bigAvatar' />
        </Grid>
        <Grid container spacing={3}>
          {this.state.workExp.projects.map((project, index) => {
            return (
              <Grid item xs={12} md={6} key={project.id}>
                <Typography variant='h5' gutterBottom>
                  {'Project ' + Math.floor(index + 1)}
                </Typography>
                <Box className='project-card' key={project.id}>
                  <Typography variant='h4' gutterBottom className='project-title'>
                    {project.name}
                  </Typography>
                  <Typography variant='subtitle2' gutterBottom>
                    {project.description}
                  </Typography>

                  <Typography variant='h6' gutterBottom>
                    Project Type
                  <ListData data={project.type} />
                  </Typography>

                  <Typography variant='h6' gutterBottom>
                    Time Period
                  <ListData data={project.timeSpan} />
                  </Typography>
                  <Typography variant='h6' gutterBottom>
                    Team Size
                  <ListData data={project.teamSize} />
                  </Typography>
                  <Typography variant='h6' gutterBottom>
                    Technologies
                  {project.technology.map((tech, index) => (
                      <ListData data={tech} key={'tech' + index} />
                    ))}
                  </Typography>
                  <Typography variant='h6' gutterBottom>
                    Roles
                  {project.role.map((roles, index) => (
                      <ListData data={roles} key={index + 'roles'} />
                    ))}
                  </Typography>
                </Box>
              </Grid>
            );
          })}
        </Grid>
        <CustomButton btnName='certifications' type='yellow' />
      </Container>
    );
  }
}

export default WorkExp;
