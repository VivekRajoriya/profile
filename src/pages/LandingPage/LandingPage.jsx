import React from 'react';
import Grid from '@material-ui/core/Grid';
import './LandingPage.scss';
import Container from '@material-ui/core/Container';
import FlipedCard from './FlipedCard/FlipedCard';
// import Chart from './Chart/Chart';
import CustomButton from '../../components/CustomButton/CustomButton';
import ConnectWithMe from '../../components/ConnectWithMe/ConnectWithMe';
import { Typography } from '@material-ui/core';

class LandingPage extends React.PureComponent {
  render() {
    return (
      <Container className='Landing-page-container'>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={5}>
            <FlipedCard />
          </Grid>
          <Grid item xs={12} sm={7} className='mobile-part-chart'>
            {/* <Chart /> */}
            <Typography variant="h3" align="center" className="Landing-text Shadow-text" >
              "&nbsp;Instead of Baby showers, let’s host a Business shower. When a friend starts a business, we all come together, celebrate them & bring resources to their business.&nbsp;"     - Elon Musk
            </Typography>
            <CustomButton btnName='profile' type='default' />
          </Grid>
          <Grid item xs={12} sm={12}>
            <ConnectWithMe title='Connect with me' />
          </Grid>
        </Grid>
      </Container>
    );
  }
}

export default LandingPage;
