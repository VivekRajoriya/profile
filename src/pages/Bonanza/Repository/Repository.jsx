import React from 'react';
import LaunchTwoToneIcon from '@material-ui/icons/LaunchTwoTone';
import { Typography, Box } from '@material-ui/core';
import '../Bonanza.scss';

export default class Repository extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      repos: [
        {
          projectTitle: 'Node API',
          link: 'https://github.com/sanju-developer/Node-Api-s'
        },
        {
          projectTitle: 'Node CRUD Operation',
          link: 'https://github.com/sanju-developer/canteen-backend'
        },
        {
          projectTitle: 'Profile',
          link: 'https://gitlab.com/VivekRajoriya/profile'
        },
        {
          projectTitle: 'Music',
          link: 'https://github.com/sanju-developer/React-Music'
        }
      ]
    };
  }

  render() {
    return (
      <>
        <Typography variant='h4' gutterBottom>
          Public Repositories
        </Typography>
        <Box className='repository-container modifier'>
          {this.state.repos.map(data => {
            return (
              <>
                <Typography variant='h6' gutterBottom>
                  {data.projectTitle}:
                </Typography>
                <Typography variant='body1' gutterBottom>
                  {data.link}
                  <span className='android-apk'>
                    <LaunchTwoToneIcon
                      onClick={() => window.open(data.link, '_blank')}
                    />
                  </span>
                </Typography>
              </>
            );
          })}
        </Box>
      </>
    );
  }
}
