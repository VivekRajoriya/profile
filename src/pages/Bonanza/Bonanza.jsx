import React from 'react';
import { Container } from '@material-ui/core';
import './Bonanza.scss';
import LiveProjects from './LiveProjects/LiveProjects';
import Devops from './Devops/Devops';
import Repository from './Repository/Repository';

export default class Bonanza extends React.PureComponent {
  render() {
    return (
      <Container className='Bonanza-container'>
        <LiveProjects />
        <Devops />
        <Repository />
      </Container>
    );
  }
}
