import React from 'react';
import {
  Box,
  Typography,
  List,
  ListSubheader,
  ListItem,
  ListItemText
} from '@material-ui/core';
import '../Bonanza.scss';

export default class Devops extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      devOps: [
        {
          title: 'Gitlab',
          work: [
            '- Create project repo on gitlab',
            '- Create .gitlab-ci.yml file for CI/CD',
            ' Assignment of free gitlab runner for CI/CD task',
            '- Hosting site on gitlab pages'
          ]
        },
        {
          title: 'Firebase',
          work: [
            '- Create project on firebase console',
            '- Integrate sign-in method like: Phone, Email/password, Google and managing users from provided Dashbaord',
            '- Hosting PWA on firebase'
          ]
        },
        {
          title: 'Github',
          work: [
            'Create repo on github',
            'Integration with github-pages via github termianl',
            'Hosting of web applications'
          ]
        },
        {
          title: 'Heroku',
          work: [
            'Project registeration on heroku console',
            'Hosting API endpoints on heroku and used by frontend-developer'
          ]
        }
      ]
    };
  }

  render() {
    return (
      <Box className='devops-container'>
        <Typography variant='h4' gutterBottom>
          Basic DevOps
        </Typography>
        {this.state.devOps.map(data => {
          return (
            <List
              className='devops-inner-container modifier'
              subheader={<ListSubheader>{data.title}</ListSubheader>}>
              {data.work.map(workName => {
                return (
                  <ListItem>
                    <ListItemText
                      id='switch-list-label-wifi'
                      primary={workName}
                    />
                  </ListItem>
                );
              })}
            </List>
          );
        })}
      </Box>
    );
  }
}
