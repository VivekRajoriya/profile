import React from 'react';
import LaunchTwoToneIcon from '@material-ui/icons/LaunchTwoTone';
import { Box, Typography, Grid } from '@material-ui/core';
import AndroidTwoToneIcon from '@material-ui/icons/AndroidTwoTone';
import ConnectWithMe from '../../../components/ConnectWithMe/ConnectWithMe';
import AppleIcon from '@material-ui/icons/Apple';
import WebTwoToneIcon from '@material-ui/icons/WebTwoTone';
import '../Bonanza.scss';

export default class LiveProjects extends React.PureComponent {
  render() {
    return (
      <Box className='live-project-container'>
        <Typography variant='h4' gutterBottom>
          Live Projects
        </Typography>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={4}>
            <Box className='inner-project-live-music-container modifier'>
              <Typography variant='h6' gutterBottom>
                Music ( Web App )
                <span className='android-apk'>
                  <WebTwoToneIcon />
                </span>
              </Typography>
              <Typography variant='body2' gutterBottom>
                It is an music web applciation for listening music, User have to
                enter the artist name for listening and then press search. On
                behalf of searching item top 10 listed songs will be presented
                by spotify and you can play any song by simply click on it. Each
                song is about of 20 sec because spotify api integrtion is build
                up on free subscription.
                <br />
                <br />
                Live URL:
                https://sanju-developer.github.io/React-Music/#/entry/music
                <span
                  className='open-new-tab-icon'
                  style={{ position: 'absolute', right: 22 }}
                  onClick={() =>
                    window.open(
                      'https://sanju-developer.github.io/React-Music/#/entry/music',
                      '_blank'
                    )
                  }>
                  <LaunchTwoToneIcon />
                </span>
              </Typography>
              <br />

              <Typography variant='h6' gutterBottom>
                Team
              </Typography>
              <Typography variant='subtitle2' gutterBottom>
                Self
              </Typography>
              <Typography variant='h6' gutterBottom>
                Technology
              </Typography>
              <Typography variant='subtitle2' gutterBottom>
                <Typography>- React JS</Typography>
                <Typography>- Redux</Typography>
                <Typography>- API integration with SPOTIFY Wrapper</Typography>
              </Typography>

              <Typography variant='h6' gutterBottom>
                DevOps
              </Typography>
              <Typography variant='subtitle2' gutterBottom>
                <Typography>- Uploaded code on github</Typography>
                <Typography>- Integration with github-pages</Typography>
                <Typography>- Application hosted to github-pages</Typography>
              </Typography>
            </Box>
          </Grid>
          <Grid item xs={12} sm={4}>
            <Box className='inner-project-live-profile-container modifier'>
              <Typography variant='h6' gutterBottom>
                Profile ( Web + PWA )
                <span className='android-apk'>
                  <WebTwoToneIcon />
                  <AndroidTwoToneIcon />
                  <AppleIcon />
                </span>
              </Typography>
              <Typography variant='body2' gutterBottom>
                I have made this as an alternative of resume. Here you can find
                more detail about candidate like my current working profile,
                skill-set, academics report, certifications, work-experience and
                much more in bonanza section. Here you can Connect with me by
                simply click on `ConnectWithMe` button (from sidebar and home
                section)
                <br />
                <br />
                Web live URL : https://vivekrajoriya.gitlab.io/profile/
                <span
                  className='open-new-tab-icon'
                  style={{ position: 'absolute', right: 22 }}
                  onClick={() =>
                    window.open(
                      'https://vivekrajoriya.gitlab.io/profile/',
                      '_blank'
                    )
                  }>
                  <LaunchTwoToneIcon />
                </span>
              </Typography>
              <Typography variant='body2' gutterBottom>
                PWA live URL : https://vivekrajoriya-pwa.firebaseapp.com/
                <span
                  className='open-new-tab-icon'
                  style={{ position: 'absolute', right: 22 }}
                  onClick={() =>
                    window.open(
                      'https://vivekrajoriya-pwa.firebaseapp.com/',
                      '_blank'
                    )
                  }>
                  <LaunchTwoToneIcon />
                </span>
              </Typography>
              <br />
              <Typography variant='h6' gutterBottom>
                Team
              </Typography>
              <Typography variant='subtitle2' gutterBottom>
                Self
              </Typography>
              <Typography variant='h6' gutterBottom>
                Technology
              </Typography>
              <Typography variant='subtitle2' gutterBottom>
                <Typography>- React JS</Typography>
                <Typography>- Recharts</Typography>
                <Typography>- Material Ui</Typography>
                <Typography>- React Spring (For Animation)</Typography>
              </Typography>

              <Typography variant='h6' gutterBottom>
                DevOps
              </Typography>
              <Typography variant='subtitle2' gutterBottom>
                <Typography>- Uploaded code on github && gitlab</Typography>
                <Typography>- Create CI/CD pipeline on gitlab</Typography>
                <Typography>- Create .yml file for gitlab runner</Typography>
                <Typography>- Application hosted to gitLab</Typography>
                <Typography>- Integration with firebase</Typography>
                <Typography>- PWA hosted to firebase</Typography>
              </Typography>
            </Box>
          </Grid>
          <Grid item xs={12} sm={4}>
            <Box className='inner-project-live-tiffin-box-container modifier'>
              <Typography variant='h6' gutterBottom>
                Tiffin Box ( Mobile App )
                <span className='android-apk'>
                  <AndroidTwoToneIcon />
                </span>
              </Typography>
              <Typography variant='body2' gutterBottom>
                We just made it for point of intreset by keeping in mind how
                food-app look like and how it works, I have created home,
                search, offer, my order, account section. Several API are
                intgerated.
                <br />
                <br />
                For android apk you can ask me on to share with you.
              </Typography>
              <ConnectWithMe title='Ask for apk' />
              <br />
              <Typography variant='h6' gutterBottom>
                Team
              </Typography>
              <Typography variant='subtitle2' gutterBottom>
                Me + backend guy
              </Typography>
              <Typography variant='h6' gutterBottom>
                Technology
              </Typography>
              <Typography variant='subtitle2' gutterBottom>
                <Typography>- Ionic</Typography>
                <Typography>- Angular</Typography>
                <Typography>
                  - firebase (for phone as a sign-in method)
                </Typography>
                <Typography>- Nodejs</Typography>
                <Typography>- Mongodb</Typography>
              </Typography>

              <Typography variant='h6' gutterBottom>
                DevOps
              </Typography>
              <Typography variant='subtitle2' gutterBottom>
                <Typography>- Use M Lab for database access</Typography>
                <Typography>- Integration with heroku</Typography>
                <Typography>- Provide API endpoint via heroku</Typography>
              </Typography>
            </Box>
          </Grid>
        </Grid>
      </Box>
    );
  }
}
