import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import PersonPinTwoToneIcon from '@material-ui/icons/PersonPinTwoTone';
import LocalPhoneTwoToneIcon from '@material-ui/icons/LocalPhoneTwoTone';
import EmailTwoToneIcon from '@material-ui/icons/EmailTwoTone';
import BusinessTwoToneIcon from '@material-ui/icons/BusinessTwoTone';
import ContactMailTwoToneIcon from '@material-ui/icons/ContactMailTwoTone';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import { darkBlueWithGrey } from '../../../assets/colors.scss';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import FileCopyTwoToneIcon from '@material-ui/icons/FileCopyTwoTone';
import { Link } from '@material-ui/core';
import '../Profile.scss';
import ShowAlert from '../../../components/Alert/Alert';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
    color: darkBlueWithGrey
  },
  profileFields: {
    maxWidth: '300px'
  },
  link: {
    margin: theme.spacing(1)
  }
}));

function ProfileInfo() {
  const [copied, setCopied] = useState(false);
  const classes = useStyles();

  return (
    <>
      <List
        subheader={<ListSubheader>Personal Information</ListSubheader>}
        className={`${classes.root}`}>
        <ListItem className='text-on-hover-description'>
          <ListItemText
            id='description'
            primary='Initially I was not in contact with computers but during my graduation/college, first time I write my code for "Hello World" and there was errors more than the number of line of code I have done. But later on I start digging it and start solving puzzels. Love to work in morning with coffee and fresh air yeah...😀. I am innovative, I try to think something out of the box. Very happy and ready to know how actually computer science works behind the scene'
          />
        </ListItem>
        <ListItem className='text-on-hover-description'>
          <ListItemText
            id='description'
            primary="Target is to create service for people, for free or open source to make them happy and make thier life easy. Always ready to learn new technologies and new stacks. One lack is if somebody ask for help, I go for it without having second thought in mind."
          />
        </ListItem>
        <ListItem className='text-on-hover web-profile'>
          <ListItemIcon>
            <PersonPinTwoToneIcon />
          </ListItemIcon>
          <ListItemText
            id='switch-list-label-bluetooth'
            primary='Designation'
            className={`${classes.profileFields}`}
          />
          <ListItemText>Frontend Engineer</ListItemText>
        </ListItem>
        <ListItem className='text-on-hover web-profile'>
          <ListItemIcon>
            <LocalPhoneTwoToneIcon />
          </ListItemIcon>
          <ListItemText
            id='switch-list-label-phone'
            primary='Phone'
            className={`${classes.profileFields}`}
          />
          <ListItemText>+91 9999262312</ListItemText>
        </ListItem>
        <ListItem className='text-on-hover web-profile '>
          <ListItemIcon>
            <EmailTwoToneIcon />
          </ListItemIcon>
          <ListItemText
            id='switch-list-label-email'
            primary='Email'
            className={`${classes.profileFields}`}
          />
          <ListItemText>
            vivekrajoriya106@gmail.com
            <CopyToClipboard
              text='vivekrajoriya106@gmail.com'
              onCopy={() => {
                setCopied(true);
                setTimeout(() => {
                  setCopied(false);
                }, 100);
              }}>
              <span className='copy-icon'>
                <FileCopyTwoToneIcon />
              </span>
            </CopyToClipboard>
          </ListItemText>
        </ListItem>
        <ListItem className='text-on-hover web-profile'>
          <ListItemIcon>
            <ContactMailTwoToneIcon />
          </ListItemIcon>
          <ListItemText
            id='switch-list-label-altermnate-email'
            primary='Alternate Email'
            className={`${classes.profileFields}`}
          />
          <ListItemText>
            sanjuparjapati1997@gmail.com
            <CopyToClipboard
              text='sanjuparjapati1997@gmail.com'
              onCopy={() => {
                setCopied(true);
                setTimeout(() => {
                  setCopied(false);
                }, 100);
              }}>
              <span className='copy-icon'>
                <FileCopyTwoToneIcon />
              </span>
            </CopyToClipboard>
          </ListItemText>
        </ListItem>
        <ListItem className='text-on-hover web-profile'>
          <ListItemIcon>
            <BusinessTwoToneIcon />
          </ListItemIcon>
          <ListItemText
            id='switch-list-label-address'
            primary='Address'
            className={`${classes.profileFields}`}
          />
          <ListItemText>
            2310, Jawahar colony NIT Faridabad, Air force road
          </ListItemText>
        </ListItem>
        <ListItem className='text-on-hover web-profile'>
          <ListItemIcon>
            <LinkedInIcon />
          </ListItemIcon>
          <ListItemText
            id='switch-list-label-linkedin'
            primary='LinkedIn'
            className={`${classes.profileFields}`}
          />
          <ListItemText>
            <Link
              href='#'
              onClick={() =>
                window.open(
                  'https://www.linkedin.com/in/vivek-rajoriya-9342a9150'
                )
              }
              className={classes.link}>
              https://www.linkedin.com/in/vivek-rajoriya-9342a9150
            </Link>
            <CopyToClipboard
              text='https://www.linkedin.com/in/vivek-rajoriya-9342a9150'
              onCopy={() => {
                setCopied(true);
                setTimeout(() => {
                  setCopied(false);
                }, 100);
              }}>
              <span className='copy-icon'>
                <FileCopyTwoToneIcon />
              </span>
            </CopyToClipboard>
          </ListItemText>
        </ListItem>
      </List>
      {copied ? <ShowAlert msg='Copied' type='success' /> : copied}
    </>
  );
}

export default ProfileInfo;
