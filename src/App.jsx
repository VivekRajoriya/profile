import React from 'react';
import './App.Scss';
import Header from './components/Header/Header';
import Routing from './routes/routing';
import history from './history';
import { SnackbarProvider } from 'notistack';

function App() {
  return (
    <SnackbarProvider
      maxSnack={3}
      autoHideDuration={2000}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
      <div className='App' style={{ width: '100%', height: '100%' }}>
        <Header history={history} />
        <Routing />
      </div>
    </SnackbarProvider>
  );
}

export default App;
