export const projectConstant = {
    workExp: {
        projects: [
            {
                id: 'proj6',
                name: 'Livia RX (Covid-19 app)',
                type: 'Web Application',
                technology: ['React-big-calendar', 'firebase', 'React-semantic-ui', 'React-datepicker', 'Moment js', 'Reactjs', 'Redux', 'php'],
                timeSpan: '3 months',
                description: `This app is build for USA. Livia RX is basically a pharmacy based application which provide COVID 19 vaccination to their patient. Pharmacy have another portal from where patient can create their appointment for that pharmacy. On the day of appointment pharmacy will provide vaccine doses. pharmacy can cancel and reschedule their appointment as well, if they are underdose.`,
                role: [
                    '- React big calendar integration',
                    '- Working hours facility to add time availability of pharmacy',
                    '- Dose 1 and Dose 2 booking facility',
                    '- Inventory management of vaccines',
                    '- Integrate firebase analytics',
                    '- Waitlisting feature of patients',
                    '- Check in/out functionality',
                    '- Cancel and reschedule appointment functionality',
                    '- Google map intgeration',
                    '- Image/docs upload/delete functionality',
                    '- Searching, filter and sorting functionality',
                    '- Otp verification functionality',
                    '- 5-step registration process with maintaining states',
                    '- Responsive layout from Laptop screen to tablet'
                ],
                teamSize: '2 (1 Frontend + 1 Backend)',
                liveUrl: 'https://labs.liviarx.com/#/'
            },
            {
                id: 'proj7',
                name: 'Livia Connect (Covid-19 app)',
                type: 'Web Application',
                technology: ['Typescript', 'firebase', 'React-helmet', 'React-semantic-ui', 'React-datepicker', 'Moment js', 'Reactjs', 'Redux', 'php'],
                timeSpan: '2 months',
                description: `This app is build for USA. Livia connect is basically a patient based application from where patient can create their appointment for any listed pharmacy. Patient can cancel and reschedule their appointment as well.`,
                role: [
                    '- Dose 1 and Dose 2 booking facility',
                    '- Google map intgeration',
                    '- Integrate firebase analytics',
                    '- Image/docs upload/delete functionality',
                    '- Cancel and reschedule appointment functionality',
                    '- Searching, filter and sorting functionality',
                    '- Otp verification functionality',
                    '- login/singup + google/facebook login.',
                    '- Responsive layout from Laptop screen to tablet'
                ],
                teamSize: '3 (2 Frontend + 1 Backend)',
                liveUrl: 'https://labs.connect.com/#/'
            },
            {
                id: 'proj5',
                name: 'Lab Portal (Health app)',
                type: 'Web Application',
                technology: ['Reactjs', 'Redux', 'php', 'React-semantic-ui', 'React-datepicker', 'Moment js'],
                timeSpan: '6 months',
                description: `This app is build for kenya. Lab portal is a platform where patient can book their appointment for any laboratory test. In kenya almost laboratory will be registered here and patient will book their appointment regarding any lab test. Charges will be taken on behalf of insurance that patient have.`,
                role: [
                    '- Create custom refresh token Interceptor',
                    '- Create reusable components',
                    '- Google map intgeration',
                    '- Image/docs upload/delete functionality.',
                    '- Searching, filter and sorting functionality',
                    '- Otp verification functionality',
                    '- 4-step registration process with maintaining states',
                    '- Responsive layout from Laptop screen to tablet'
                ],
                teamSize: '2 (1 Frontend + 1 Backend)',
                liveUrl: 'https://labs.liviaapp.com/#/'
            },
            {
                id: 'proj8',
                name: 'Livia hospital (Health app)',
                type: 'Web Application',
                technology: ['Amazon chime sdk', 'React full calendar', 'Reactjs', 'Redux', 'php', 'React-semantic-ui', 'React-datepicker', 'Moment js'],
                timeSpan: '5 months + ongoing',
                description: `This app is build for kenya. Livia hospital is platform where doctor attend their patient and create preAuth, prescription, e-claims for them. Here doctors have ability to add patient in their listing, can schedule appointment for them and many more.`,
                role: [
                    '- Amazon chime integration',
                    '- React big calendar integration',
                    '- Preauth creation to create claim for insurance companies.',
                    '- Doctor prescription feature',
                    '- Google map intgeration',
                    '- Role managment (admin, receptionist, doctor, manager)',
                    '- Appointment mamnagement like serve, cacnel and delete appointment',
                    '- Create custom refresh token Interceptor',
                    '- Create reusable components',
                    '- Image/docs upload/delete functionality.',
                    '- Searching, filter and sorting functionality',
                    '- Otp verification functionality',
                    '- 2-step registration process with maintaining states',
                    '- Responsive layout from Laptop screen to tablet'
                ],
                teamSize: '3 (2 Frontend + 1 Backend)',
            },
            {
                id: 'proj9',
                name: 'Subscription Manager (It is a platform to manage payment of subscription.)',
                type: 'Web Application',
                technology: ['Braintree sdk', 'React-intl', 'Reactjs', 'Redux', 'Python', 'Reactstrap'],
                timeSpan: '5 months',
                description: `It is platform where user can choose their plan regarding subscription and then can buy/renew it.`,
                role: [
                    '- Integration of Braintree payment gateway (Add/delete/Edit',
                    '- 3-step order place flow from order->payment->deliver',
                    '- Integrate change of language functionality',
                    '- Redirection from one website (SM) to another(GSC)',
                    '- Create new designs from PSD and added functionality',
                ],
                teamSize: '3 (2 Frontend + 1 Backend)',
                liveUrl: 'https://awesome-billing.gappsexperts.com/accounts/login/?next=/'
            },
            {
                id: 'proj1',
                name: 'Rewhiz (SaaS)',
                type: 'Web Application',
                technology: ['Reactjs', 'Redux', 'Web socket', 'Socail login', 'Python', 'React-bootstrap'],
                timeSpan: '4 months (on Hold)',
                description: `Rewhiz ia a platform to manage SaaS subscriptions. Rewhiz is an subscription management application, where user can subscribe for the new subscription and update their exisiting subscription. With this platform, customers will no longer need to deal with several vendors for their invoices, payments, licenses management etc?
              It's a tool to manage what type of applications/subscriptions are using within an organization by their employess. Admin can ask for the justification of a subscription/application like "why do you install this application" or can delete the application from his/her system.`,
                role: [
                    '- Integration of Web socket for notification',
                    '- Create new designs from PSD and added functionality',
                    '- Create custom refresh token Interceptor (challenging task)',
                    '- Integrate virtual scroll',
                    '- Integrate infinite sroll',
                    '- Integrate Google login',
                    '- Custom responsive Table which added table columns in between with passage of time',
                ],
                teamSize: '2 (1 Frontend + 1 Backend)'
            },
            {
                id: 'proj2',
                name: 'Woodstock (Inventory managment)',
                description:
                    'Woodstock is a platform for maintaining their different types of stock of wood. Employee allow to manage stock with this applciation by adding, deleting, updating records. Application contain different set of roles and each employee having work regarding their role.',
                type: 'Web Application + Mobile App',
                technology: [
                    'Ionic',
                    'PWA',
                    'php/Laravel',
                    'Rxjs',
                    'Angular material'
                ],
                timeSpan: '5 months',
                teamSize: '2 (2 Frontend + Backend by client)',
                role: [
                    '- Integrate NgRx',
                    '- Infinite Scroll Implementation',
                    '- Virtual scroll Implementation',
                    '- Create new designs from PSD and added functionality',
                    '- Bug fixes',
                    '- Build for Android adn PWA'
                ]
            },
            {
                id: 'proj3',
                name: 'Invoice System',
                type: 'Web Application',
                technology: ['Angular', 'Nodejs'],
                timeSpan: '3 months',
                description:
                    'Invoice application is used to generate invoices based on project type. If the project is of Indian client then there will be invoice which folow indian rules like GST, else the rules based on country type. Application have bank section and only admin is allow to view this section, which holds the record where the ammount would be received. This was an internal project for the organization.',
                role: [
                    '- Generate design from PSD',
                    '- Add print functionalty',
                    '- Added logic for GST calculation (given by client)',
                    '- Build live on firebase for testing'
                ],
                teamSize: '2 (1 Frontend + 1 Backend )'
            },
            {
                id: 'proj4',
                name: 'Albaap',
                type: 'Mobile Application',
                technology: ['Ionic', 'Nodejs', 'Rxjs'],
                timeSpan: '1 months',
                description:
                    'Albaap application to request the professional home maintenance services. It is the most convenient way to ask for help in any maintenance requirements faced n a builing. User has to raised a request and then from Albaap team price is listed, then user can negotiate it for futher process otherwise cancel it. Services like Plumbing, Electricican, Gas Services etc.',
                role: [
                    '- Integrate google map',
                    '- Create build for IOS and Android',
                    '- Build live on Playstore',
                    '- Build live on AppStore',
                    '- Bug fixes'
                ],
                teamSize: '2 (1 Frontend + 1 Backend )'
            }
        ],
        companies: [
            { name: 'Appinventiv Technology Pvt. Ltd', servedTime: 'October/2020 - till present' },
            { name: 'Yugasa software labs', servedTime: '10 months' },
            {
                name: 'Daffodils software private limited',
                servedTime: '1.8yr'
            }
        ]
    }
}